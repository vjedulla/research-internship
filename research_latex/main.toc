\contentsline {title}{Approximating support using probabilistic data structures}{1}{section*.2}
\contentsline {abstract}{Abstract}{1}{section*.1}
\tocdepth@munge 
\contentsline {section}{\numberline {}Contents}{1}{section*.3}
\tocdepth@restore 
\contentsline {section}{\numberline {}List of Figures}{1}{section*.4}
\contentsline {section}{\numberline {}List of Tables}{1}{section*.5}
\contentsline {section}{\numberline {I}Introduction}{3}{section*.6}
\contentsline {subsection}{\numberline {A}Organization of the paper}{3}{section*.7}
\contentsline {subsection}{\numberline {B}Aims and objectives}{3}{section*.8}
\contentsline {section}{\numberline {II}Prelude}{3}{section*.9}
\contentsline {subsection}{\numberline {A}Preliminaries}{3}{section*.10}
\contentsline {subsection}{\numberline {B}Problem statement}{3}{section*.11}
\contentsline {section}{\numberline {III}Algorithm \& Data structures}{4}{section*.12}
\contentsline {subsection}{\numberline {A}Datasets}{4}{section*.13}
\contentsline {subsection}{\numberline {B}Eclat Algorithm}{4}{section*.14}
\contentsline {subsection}{\numberline {C}Eclat variation}{5}{section*.15}
\contentsline {subsection}{\numberline {D}Query for support}{5}{section*.16}
\contentsline {subsection}{\numberline {E}Bloom filters}{5}{section*.17}
\contentsline {subsubsection}{\numberline {1}Approximating size using Bloom Filters}{6}{section*.18}
\contentsline {subsubsection}{\numberline {2}Problems in bloom filter}{6}{section*.19}
\contentsline {subsection}{\numberline {F}HyperLogLog}{6}{section*.20}
\contentsline {section}{\numberline {IV}Statistics}{7}{section*.21}
\contentsline {subsubsection}{\numberline {1}Aggregated statistics}{7}{section*.22}
\contentsline {subsubsection}{\numberline {2}Missing from the approximator}{7}{section*.23}
\contentsline {subsubsection}{\numberline {3}Extra on the approximator}{7}{section*.24}
\contentsline {subsubsection}{\numberline {4}Best fit line on error}{7}{section*.25}
\contentsline {subsubsection}{\numberline {5}Error difference}{8}{section*.26}
\contentsline {subsubsection}{\numberline {6}Hash functions}{8}{section*.27}
\contentsline {subsubsection}{\numberline {7}Parent child error difference}{8}{section*.28}
\contentsline {section}{\numberline {V}Experiments}{9}{section*.29}
\contentsline {subsection}{\numberline {A}Configuration of experiments}{9}{section*.30}
\contentsline {subsection}{\numberline {B}Runs}{9}{section*.31}
\contentsline {subsection}{\numberline {C}Good runs}{10}{section*.32}
\contentsline {subsubsection}{\numberline {1}Accidents dataset}{10}{section*.33}
\contentsline {subsubsection}{\numberline {2}Census Income dataset}{12}{section*.34}
\contentsline {subsection}{\numberline {D}False positive runs}{13}{section*.35}
\contentsline {subsubsection}{\numberline {1}Mushroom dataset}{13}{section*.36}
\contentsline {subsection}{\numberline {E}Unstable runs}{14}{section*.37}
\contentsline {subsubsection}{\numberline {1}Chess dataset}{14}{section*.38}
\contentsline {subsection}{\numberline {F}Timing}{16}{section*.39}
\contentsline {subsection}{\numberline {G}Initialization time}{17}{section*.40}
\contentsline {section}{\numberline {VI}Conclusions}{17}{section*.41}
\contentsline {section}{\numberline {VII} Implementation details}{18}{section*.42}
\contentsline {subsection}{\numberline {A}Main invocation}{18}{section*.43}
\contentsline {subsection}{\numberline {B}Statistics files}{18}{section*.44}
