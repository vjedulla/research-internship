import numpy as np
import pandas as pd
import utils

import matplotlib.pyplot as plt

available = ['accidents', 'chess', 'connect', 'generated', 'kosarak', 'mushroom', 'pumsb_star', 'pumsb', 'retail', ]

files = utils.get_files(['accidents '])

dfs = []
for each in files:
    dfs.append(pd.read_csv(each, index_col=0))

data = []
level_divider = []
max_levels = 0
who = 0
cnt = 0
for df in dfs:
    lev = 0
    for i in range(len(df["level"].values) - 1):
        item = df["level"].values[i]
        item_next = df["level"].values[i + 1]
        if item != item_next and i not in level_divider:
            level_divider.append(i + 1)
            lev += 1

    if lev > max_levels:
        who = cnt
        max_levels = lev

    data.append(df['error'].values)
    cnt += 1

nd_arr = np.array(data)

# print(who, max_levels)

# print(level_divider)
# print(len(level_divider))
# print(len(nd_arr))
# print(len(nd_arr[0]))
# print(len(nd_arr[1]))
# print(len(nd_arr[2]))

arr = np.ma.empty((len(nd_arr),max([len(a) for a in nd_arr])))

arr.mask = True
for i in range(0, len(nd_arr)):
    arr[i][:nd_arr[i].shape[0]] = nd_arr[i]


# print(arr)
mean = arr.mean(axis=0)


plt.plot(mean, 'o-', color="red", label="error difference", markersize=1)
plt.ylabel('support error from exact')
plt.xlabel('items')

x_axis = np.arange(0, len(mean), 1)

for div in level_divider:
    plt.axvline(x=div, color="green")

fitt = np.polyfit(x_axis, mean, 1)
print("slope:", fitt[0])
poly = np.poly1d(fitt)
plt.plot(poly(x_axis), '-', color="purple", label="fit line", markersize=3)
plt.legend()
plt.grid()
plt.show()

