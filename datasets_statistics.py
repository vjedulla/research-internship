import utils
import numpy as np
import pandas as pd

def count_statistics(filename):
    tid = 0
    key_count = 0
    with open(utils.base_dataset_dir + filename, "r") as input_file:
        # read data
        for row in input_file:
            string = row.split()
            tid += 1
            for key in string:
                if key not in dictionary:
                    dictionary[key] = [tid]
                    key_count += 1
                else:
                    dictionary[key].append(tid)

    stats = []

    for key in dictionary:
        stats.append((len(dictionary[key]), key))

    minimum = np.min(stats)
    maximum = np.max(stats)
    ave = np.average(stats)

    dictionary.clear()

    return filename, tid, key_count, minimum, round(ave, 2), maximum


dictionary = {}
datasets = utils.get_datasets()
stats = []
for dataset in datasets:
    print('Counting dataset {0}...'.format(dataset))
    stats.append((count_statistics(dataset)))

df = pd.DataFrame(columns=['filename', 'rows', 'items', 'min(tid_lst)', 'ave(tid_lst)', 'max(tid_lst)'])
df['filename'], df["rows"], df["items"], df["min(tid_lst)"], \
    df["ave(tid_lst)"], df["max(tid_lst)"] = zip(*stats)


df.to_csv('stats/datasets_statistics.csv')
