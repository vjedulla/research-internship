import numpy as np
import matplotlib.pyplot as plt
from pybloom import BloomFilter


bf = BloomFilter(capacity=3000, error_rate=0.15)
m = bf.bits_per_slice
k = bf.num_slices
print("m", bf.bits_per_slice)
print("k", bf.num_slices)


def f(variable):
    return -(m / k) * np.log(1 - (variable / m))

plot1 = np.arange(1, m-1, 1)
res1 = np.apply_along_axis(f, 0, plot1)
gradient1 = np.gradient(res1)


print(f(m-1))
print(f(m-2))


plt.figure(1)
plt.subplot(111)
plt.axvline(x=m, linestyle='--', color='red', label="bitarray length m")
plt.axvline(x=m-(m*0.05), linestyle='--', color='red', label="bitarray length m")
plt.plot(plot1, gradient1, '-', color='blue', label='bits set to 1: X from -> 1 to ' + str(m-1))

plt.ylabel('approximation')
plt.xlabel('variable X bits set to 1')

plt.legend()
plt.show()