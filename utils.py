from wrapper import ApproxAlgorithmWrapper
import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os
import pybloom


def get_support(list_found, minsup, is_approx="exact", for_approach="bloom"):
    for entry in list_found:
        intersection = None
        for item in entry['items']:
            if intersection is None:
                if is_approx is "exact":
                    intersection = data[item]
                elif is_approx is "bloom":
                    intersection = approx_bloom[item]
                elif is_approx is "hll":
                    intersection = approx_hll[item]
            else:
                if is_approx is "exact":
                    intersection = intersect(intersection, data[item], is_approx=is_approx, a=True)
                elif is_approx is "bloom":
                    intersection = intersect(intersection, approx_bloom[item], is_approx=is_approx)
                elif is_approx is "hll":
                    intersection = intersect(intersection, approx_hll[item], is_approx=is_approx)

        real_support = len(intersection)

        if real_support < minsup:
            if is_approx is "exact":
                found_exact_support_of_approximated_items(entry['items'], real_support, entry['support'], entry['level'],
                                                          entry['filled'], is_approx=is_approx, for_approach=for_approach)
            else:
                found_exact_support_of_approximated_items(entry['items'], entry['support'], real_support,
                                                          entry['level'], entry['filled'], is_approx=is_approx)

    if is_approx is "exact":
        if for_approach is "bloom":
            return found_extra_elements_bloom
        elif for_approach is "hll":
            return found_extra_elements_hll
    elif is_approx is "bloom":
        return found_missing_elements_bloom
    elif is_approx is "hll":
        return found_missing_elements_hll

def get_files(folders, specific='bloom', pattern='errors_'):
    from os import listdir
    from os.path import isfile, join
    files = []
    for folder in folders:
        path = base_stats_dir + folder + '/' + specific
        for f in listdir(path):
            if isfile(join(path, f)) and f.startswith(pattern):
                files.append(join(path, f))

    return files


def get_datasets():
    from os import listdir
    from os.path import isfile, join
    onlyfiles = [f for f in listdir(base_dataset_dir) if isfile(join(base_dataset_dir, f))]

    known_datasets_and_properties = {
        'accidents.dat' : {
            'default_minsup': 300000,
            'default_capacity': 400000,
            'default_error_rate': 0.3
        },
        'chess.dat': {
            'default_minsup': 3000,
            'default_capacity': 6190,
            'default_error_rate': 0.3,
        },
        'connect.dat': {
            'default_minsup': 65000,
            'default_capacity': 140000,
            'default_error_rate': 0.1
        },
        'generated.dat': {
            'default_minsup': 1000,
            'default_capacity': 100000,
            'default_error_rate': 0.3
        },
        'kosarak.dat': {
            'default_minsup': 100000,
            'default_capacity': 600000,
            'default_error_rate': 0.3
        },
        'mushroom.dat': {
            'default_minsup': 3000,
            'default_capacity': 10000,
            'default_error_rate': 0.3
        },
        'output.dat': {
            'default_minsup': 300,
            'default_capacity': 1000,
            'default_error_rate': 0.3
        },
        'pumsb.dat': {
            'default_minsup': 45000,
            'default_capacity': 100000,
            'default_error_rate': 0.3
        },
        'pumsb_star.dat': {
            'default_minsup': 35000,
            'default_capacity': 100000,
            'default_error_rate': 0.3
        },
        'retail.dat': {
            'default_minsup': 1000,
            'default_capacity': 500000,
            'default_error_rate': 0.3
        }
    }

    datasets_and_properties = {}

    for file in onlyfiles:
        if file in known_datasets_and_properties:
            datasets_and_properties[file] = known_datasets_and_properties[file]

    return datasets_and_properties


def intersect(first, second, is_approx="exact", a=False):
    if is_approx is "bloom" or is_approx is "hll":
        return first.intersection(second)
    else:
        #
        # if isinstance(first[0], int) and isinstance(second[0], int):
        #     return list(set(first) & set(second))
        #
        # if isinstance(first[0], int) and isinstance(second[0], list):
        #     return list(set(first) & set(second[0]))
        #
        #
        # if isinstance(first[0], list) and isinstance(second[0], int):
        #     return list(set(first[0]) & set(second))
        #
        # if isinstance(first[0], list) and isinstance(second[0], list):
        #     return list(set(first[0]) & set(second[0]))
        #
        #
        # if a:
        #     # print(type(first[0]), type(second[0]))
        #     return list(set(first[0]) & set(second[0]))

        return list(set(first) & set(second))


def union(first, second, is_approx="exact"):
    if is_approx is "bloom" or is_approx is "hll":
        return first.union(second)
    else:
        return set(first) | set(second)


def add_to_found(items, item_found, support, level, jaccard=-1, is_approx="exact", filled=0):
    obj = {
        "items": items,
        "found": item_found,
        "support": support,
        "level": level,
        "jaccard": jaccard,
        "filled": filled
    }

    if is_approx is "bloom":
        if level == 0:
            filled_dictionary[items[0]] = filled
        found_approx_bloom.append(obj)
    elif is_approx is "hll":
        found_approx_hll.append(obj)
    else:
        found.append(obj)


def modify_found_bloom_filers_filled():
    for tmp in found_approx_bloom:
        if tmp['level'] > 0:
            filled = get_items_filled(tmp['items'])
            tmp['filled'] = filled

def get_items_filled(lst):
    dic = {}
    for item in lst:
        dic[item] = filled_dictionary[item]

    return dic


# def add_to_dictionary(dictionary, key, value, is_approx="exact", properties=None):
#     if key not in dictionary:
#         if is_approx is "bloom":
#             f = ApproxAlgorithmWrapper("py_bf", "BloomFilter", properties)
#             f.add(value)
#             if properties['calculate_jaccard']:
#                 dictionary[key] = (f, 1)
#             else:
#                 dictionary[key] = f
#         elif is_approx is "hll":
#             f = ApproxAlgorithmWrapper("hyperloglog.hll", "HyperLogLog", properties)
#             f.add(value, properties["hash_fn"])
#             if properties['calculate_jaccard']:
#                 dictionary[key] = (f, 1)
#             else:
#                 dictionary[key] = f
#         else:
#             if properties['calculate_jaccard']:
#                 dictionary[key] = ([value], 1)
#             else:
#                 dictionary[key] = [value]
#     else:
#         if is_approx is "bloom":
#             if properties['calculate_jaccard']:
#                 dictionary[key][0].add(value)
#             else:
#                 dictionary[key].add(value)
#         if is_approx is "hll":
#             if properties['calculate_jaccard']:
#                 dictionary[key][0].add(value, properties["hash_fn"])
#             else:
#                 dictionary[key].add(value, properties["hash_fn"])
#         elif is_approx is "exact":
#             if properties['calculate_jaccard']:
#                 dictionary[key][0].append(value)
#             else:
#                 dictionary[key].append(value)


def add_to_dictionary(dictionary, key, value, is_approx="exact", properties=None):
    if key not in dictionary:
        if is_approx is "bloom":
            f = ApproxAlgorithmWrapper("py_bf", "BloomFilter", properties)
            f.append(value)
            dictionary[key] = f
        else:
            dictionary[key] = [value]

    else:
        dictionary[key].append(value)

def read_data(filename, properties):
    tid = 0
    with open(base_dataset_dir + filename, "r") as input_file:
        # read data
        for row in input_file:
            string = row.split()
            tid += 1
            for key in string:
                if properties["real_or_approx"]:
                    # build the real thing
                    add_to_dictionary(data, key, tid, is_approx="exact", properties=properties)

                if not properties["real_or_approx"]:
                    if properties["bloom"] is True:
                        # build the approx bloom
                        add_to_dictionary(approx_bloom, key, tid, is_approx="bloom", properties=properties)

                    # if properties["hll"] is True:
                    #     # build the approx hll
                    #     add_to_dictionary(approx_hll, key, tid, is_approx="hll", properties=properties)

    # sort data
    items = sorted(data.items(), key=lambda item: (len(item[1]), item[0]), reverse=True)
    items_approx_bloom = sorted(approx_bloom.items(), key=lambda item: item[0], reverse=True)
    items_approx_hll = sorted(approx_hll.items(), key=lambda item: item[0], reverse=True)

    if properties["real_or_approx"]:
        return items
    else:
        return items_approx_bloom, items_approx_hll


def found_exact_support_of_approximated_items(item, real_support, approximated_support, level, filled, is_approx="exact", for_approach="bloom"):
    obj = {
        'item': item,
        'real_support': real_support,
        'approximated_support': approximated_support,
        'level': level,
        'filled': filled
    }

    if is_approx is "exact":
        if for_approach is "bloom":
            found_extra_elements_bloom.append(obj)
        elif for_approach is "hll":
            found_extra_elements_hll.append(obj)
    elif is_approx is "bloom":
        found_missing_elements_bloom.append(obj)
    elif is_approx is "hll":
        found_missing_elements_hll.append(obj)


data = {}
approx_bloom = {}
approx_hll = {}


base_dataset_dir = 'datasets/'
base_stats_dir = 'stats/'

found = []
found_approx_bloom = []
filled_dictionary = {}
found_approx_hll = []

found_extra_elements_bloom = []
found_missing_elements_bloom = []

found_extra_elements_hll = []
found_missing_elements_hll = []

class Statistics:

    def __init__(self, data, differences_list, dataset=None, experiment=None, support=None):
        self.base_dir = 'stats/' + dataset.replace('.dat', '') + '/'
        self.dataset = dataset
        self.experiment = experiment
        self.minsupport = support
        self.fig_size = (10, 8)

        all = ['bloom', 'hll']

        # create a folder for the approaches
        for which in all:
            path = self.base_dir + which
            if not os.path.isdir(path):
                os.makedirs(path, exist_ok=True)

        which = 'support'
        # which = 'level'
        col_exact = [x for x in data[0]]
        col_bloom = [x for x in data[1]]
        col_hll = [x for x in data[2]]
        extra_on_bloom_filter = [x for x in differences_list[0]]
        missing_on_bloom_filter = [x for x in differences_list[1]]
        extra_on_hll = [x for x in differences_list[2]]
        missing_on_hll = [x for x in differences_list[3]]

        col_exact.sort(key=lambda x: (x["level"], x["support"]))
        col_bloom.sort(key=lambda x: (x["level"], x["support"]))
        col_hll.sort(key=lambda x: (x["level"], x["support"]))

        self.count_bloom_filter = len(col_bloom)
        self.count_hll = len(col_hll)
        self.count_exact = len(col_exact)

        all = [
            col_exact, col_bloom, col_hll,
            extra_on_bloom_filter, missing_on_bloom_filter,
            extra_on_hll, missing_on_hll
        ]
        max_len = max([
            len(col_exact), len(col_bloom),  len(col_hll),
            len(extra_on_bloom_filter), len(missing_on_bloom_filter),
            len(extra_on_hll), len(missing_on_hll),
        ])

        # match length of the lists
        for lst in all:
            diff = max_len - len(lst)
            for _ in range(diff):
                lst.append(None)

        # general found statistics
        self.df = pd.DataFrame()
        self.df["exact"] = col_exact
        self.df["bloomfilter"] = col_bloom
        self.df["extra_on_bloom"] = extra_on_bloom_filter
        self.df["missing_on_bloom"] = missing_on_bloom_filter
        self.df["hll"] = col_hll
        self.df["extra_on_hll"] = extra_on_hll
        self.df["missing_on_hll"] = missing_on_hll

        self.df.to_csv(self.base_dir + "stats_" + self.experiment + ".csv")

        # how many are missing from the bloom filter
        self.missing_exact_bloom = self.count_missing(col_exact, col_bloom)
        # how many are extra on bloom filter
        self.missing_bloom_exact = self.count_missing(col_bloom, col_exact)

        self.missing_exact_hll = self.count_missing(col_exact, col_hll)
        self.missing_hll_exact = self.count_missing(col_hll, col_exact)

        # count the error only on found items
        self.errors_bloom = self.count_error(col_exact, col_bloom, "bloom")
        self.errors_hll = self.count_error(col_exact, col_hll, "hll")

        # count how many are missing
        self.errors_extra_bloom = self.count_diff_items_error(extra_on_bloom_filter, "bloom", type="extra")
        self.errors_missing_bloom = self.count_diff_items_error(missing_on_bloom_filter, "bloom", type="missing")

        self.errors_extra_hll = self.count_diff_items_error(extra_on_hll, "hll", type="extra")
        self.errors_missing_hll = self.count_diff_items_error(missing_on_hll, "hll", type="missing")

        # stats
        self.statistics()

        # self.calculate_relation_parent_child(self.errors_bloom, "bloom")
        # self.calculate_relation_parent_child(self.errors_hll, "bloom")

    def calculate_relation_parent_child(self, approx, which):
        if isinstance(approx, bool): return

        c1 = []
        c1_err = []
        c2 = []
        c2_err = []
        differences = []
        for i, item in approx.iterrows():
            for j, other in approx.iterrows():
                if j == i: continue

                parent = item["approx_" + which]
                child = other["approx_" + which]

                if child["level"] - parent["level"] != 1: continue

                if self.lists_match(parent["items"], child["items"][:-1]):
                    relative_error = abs(int(other["error"]) - int(item["error"]))
                    c1.append(parent)
                    c1_err.append(item["error"])
                    c2.append(child)
                    c2_err.append(other["error"])
                    differences.append(relative_error)

        df = pd.DataFrame(columns=['parent', 'child', 'relative_error'])
        df["parent"] = c1
        df["parent_error"] = c1_err
        df["child"] = c2
        df["child_error"] = c2_err
        df["relative_error"] = differences

        plt.plot(df["relative_error"].values, color="red", label="relative error")
        # if which is "bloom":
        #     plt.ylim((-(self.minsupport / 10), self.minsupport))
        # elif which is "hll":
        #     plt.ylim((-1500, 1000))

        fig = plt.gcf()
        fig.set_size_inches(self.fig_size)
        plt.savefig(self.base_dir + which + "/" + "relative_error_" + self.experiment, dpi=100)
        plt.close()

        df.to_csv(self.base_dir + which + "/" + "relative_error_" + self.experiment + ".csv")

        return df

    def statistics(self):
        if not isinstance(self.errors_bloom, bool):
            gs = gridspec.GridSpec(2, 2)
            self.mean_error_bloom = float(np.mean(self.errors_bloom["error"]))
            # self.variance_bloom = float(np.var(self.errors_bloom["error"]))
            self.std_bloom = float(np.std(self.errors_bloom["error"]))

            mse_bloom = mean_squared_error(self.errors_bloom["exact_support"], self.errors_bloom["approx_support"])

            print("----Bloom Filter-----")
            print("Missing from exact:", len(self.missing_exact_bloom))
            print("Extra on bloom:", len(self.missing_bloom_exact))
            print("False positive:", str(round(float(len(self.missing_bloom_exact) / self.count_bloom_filter) * 100.0, 3)) + "%")
            print("Min / Max error:", min([x for x in self.errors_bloom["error"].values]), '/', max([x for x in self.errors_bloom["error"].values]))
            # print("Max error:", )
            print("Mean error support:", round(self.mean_error_bloom, 3))
            print("SD error support:", round(self.std_bloom, 3))
            print("Error %:", str(round((abs(self.mean_error_bloom))/(self.minsupport) * 100, 4)) + "%")
            # print("Mean Squared Error (MSE):", round(mse_bloom, 3))
            print("# Underestimate / Overestimate:", np.count_nonzero(self.errors_bloom["error"] < 0), '/', np.count_nonzero(self.errors_bloom["error"] > 0))
            # print("# Overestimate:", )

            # ----------------------------------------------------------------------------------------------
            ax = plt.subplot(gs[0, 0])
            plt.plot(self.errors_bloom["approx_support"].values, color="red", label="bloom filter")
            plt.plot(self.errors_bloom["exact_support"].values, color="blue", label="exact")
            plt.ylabel('support')
            plt.xlabel('items (sorted on level, error)')

            level_divider = []
            for i in range(len(self.errors_bloom["exact_support"].values) - 1):
                item = self.errors_bloom["exact"].values[i]
                item_next = self.errors_bloom["exact"].values[i + 1]
                if item["level"] != item_next["level"]:
                    level_divider.append(i+1)
                    # plt.axvline(x=i+1, color="green")

            for div in level_divider:
                plt.axvline(x=div, color="green")

            plt.legend()
            plt.grid()

            # ----------------------------------------------------------------------------------------------
            ax = plt.subplot(gs[0, 1])
            plt.plot(self.errors_bloom["error"].values, 'o-', color="red", label="error difference", markersize=1)
            plt.ylabel('support error from exact')
            plt.xlabel('items (sorted on level, error)')

            x_axis = np.arange(0, len(self.errors_bloom['error'].values), 1)

            fitt = np.polyfit(x_axis, self.errors_bloom["error"], 1)
            poly = np.poly1d(fitt)
            plt.plot(poly(x_axis), '-', color="purple", label="fit line", markersize=3)

            self.store_fitt_bloom = fitt

            for div in level_divider:
                plt.axvline(x=div, color="green")

            plt.legend()
            plt.grid()

            # ----------------------------------------------------------------------------------------------
            ax = plt.subplot(gs[1, 0])
            bins = 10
            plt.hist(self.errors_bloom["error"].values, bins)

            plt.axvline(x=self.mean_error_bloom, color="red")

            plt.ylabel('number of items')
            plt.xlabel('error from exact')
            plt.grid()

            values = []
            for i in self.errors_bloom["filled"].values:

                if isinstance(i, dict):
                    tmp = min(list(i.values()))
                    # tmp = list(i.values())
                    # avg = np.average(tmp, axis=0)
                    values.append(tmp)
                else:
                    values.append(i)

            # ----------------------------------------------------------------------------------------------
            ax = plt.subplot(gs[1, 1])

            plt.plot(values, 'o-', color="red", label="% filled", markersize=1)
            plt.ylabel('filled ratio: X/m')
            plt.xlabel('items')

            for div in level_divider:
                plt.axvline(x=div, color="green")

            plt.ylim((0, 100))
            plt.legend()
            plt.grid()

            # ----------------------------------------------------------------------------------------------
            # plt.show()
            fig = plt.gcf()
            fig.set_size_inches(self.fig_size)
            plt.savefig(self.base_dir + "experiment_bloom_" + self.experiment, dpi=400)
            plt.close()

        if not isinstance(self.errors_hll, bool):
            gs = gridspec.GridSpec(2, 2)
            self.mean_error_hll = float(np.mean(self.errors_hll["error"]))
            self.std_hll = float(np.std(self.errors_hll["error"]))

            mse_hll = mean_squared_error(self.errors_hll["exact_support"], self.errors_hll["approx_support"])

            print("----HyperLogLog-----")
            print("Missing from exact:", len(self.missing_exact_hll))
            print("Extra on HLL:", len(self.missing_hll_exact))
            print("False positive:", str(round(float(len(self.missing_hll_exact) / self.count_hll) * 100.0, 3)) + "%")
            print("Min error:", min([x for x in self.errors_hll["error"].values]))
            print("Max error:", max([x for x in self.errors_hll["error"].values]))
            print("Mean error support:", round(self.mean_error_hll, 3))
            print("SD error support:", round(self.std_hll, 3))
            print("Error %:", str(round((abs(self.mean_error_hll))/(self.minsupport) * 100, 4)) + "%")
            print("Mean Squared Error (MSE):", round(mse_hll, 3))
            print("# Underestimate:", np.count_nonzero(self.errors_hll["error"] < 0))
            print("# Overestimate:", np.count_nonzero(self.errors_hll["error"] > 0))

            # ----------------------------------------------------------------------------------------------
            ax = plt.subplot(gs[0, 0])
            plt.plot(self.errors_hll["approx_support"].values, color="red", label="hyperloglog")
            plt.plot(self.errors_hll["exact_support"].values, color="blue", label="exact")
            plt.ylabel('support')
            plt.xlabel('items (sorted on level, error)')

            level_divider = []
            for i in range(len(self.errors_hll["exact_support"].values) - 1):
                item = self.errors_hll["exact"].values[i]
                item_next = self.errors_hll["exact"].values[i + 1]
                if item["level"] != item_next["level"]:
                    level_divider.append(i+1)
                    # plt.axvline(x=i+1, color="green")

            for div in level_divider:
                plt.axvline(x=div, color="green")

            plt.legend()
            plt.grid()

            # ----------------------------------------------------------------------------------------------
            ax = plt.subplot(gs[0, 1])
            plt.plot(self.errors_hll["error"].values, 'o-', color="red", label="error difference", markersize=1)
            plt.ylabel('support error from exact')
            plt.xlabel('items (sorted on level, error)')

            x_axis = np.arange(0, len(self.errors_hll['error'].values), 1)

            fitt = np.polyfit(x_axis, self.errors_hll["error"], 3)
            poly = np.poly1d(fitt)
            plt.plot(poly(x_axis), '-', color="purple", label="fit line", markersize=3)

            self.store_fitt_hll = fitt

            for div in level_divider:
                plt.axvline(x=div, color="green")

            plt.legend()
            plt.grid()

            # ----------------------------------------------------------------------------------------------
            ax = plt.subplot(gs[1, 0])
            bins = 10
            plt.hist(self.errors_hll["error"].values, bins)

            plt.axvline(x=self.mean_error_hll, color="red")

            plt.ylabel('number of items')
            plt.xlabel('error from exact')
            plt.grid()

            values = []
            for i in self.errors_hll["filled"].values:

                if isinstance(i, dict):
                    tmp = min(list(i.values()))
                    # tmp = list(i.values())
                    # avg = np.average(tmp, axis=0)
                    values.append(tmp)
                else:
                    values.append(i)

            # ----------------------------------------------------------------------------------------------
            fig = plt.gcf()
            fig.set_size_inches(self.fig_size)
            plt.savefig(self.base_dir + "experiment_hll_" + self.experiment, dpi=400)
            plt.close()


    def count_error(self, exact, approx, which):
        c1 = []
        c2 = []
        err = []
        filled_ratio = []
        for exact_item in exact:
            for approx_item in approx:
                if exact_item is not None and approx_item is not None and self.lists_match(exact_item["items"], approx_item["items"]):
                    error = -1 * (int(exact_item["support"]) - int(approx_item["support"]))
                    c1.append(exact_item)
                    c2.append(approx_item)
                    exact_support = exact_item["support"]
                    approx_support = approx_item["support"]
                    fill = approx_item["filled"]
                    level = approx_item['level']
                    jaccard_real = exact_item['jaccard']
                    jaccard_approx = approx_item['jaccard']
                    err.append((exact_support, approx_support, level, error, fill, jaccard_real, jaccard_approx))

        if len(err) == 0:
            return False

        df = pd.DataFrame(columns=['exact', 'approx_' + which])
        df['exact'] = c1
        df['approx_'+which] = c2
        df['exact_support'], df["approx_support"], df["level"], df["error"], \
            df["filled"], df["jaccard_real"], df["jaccard_approx"] = zip(*err)

        df.sort_values(['level', 'error'], inplace=True)
        df.set_index(np.arange(len(df.index)), inplace=True)

        df.to_csv(self.base_dir + which + "/" + "errors_" + self.experiment + ".csv")
        return df

    def count_diff_items_error(self, extra, which, type="extra"):
        c1 = []
        err = []

        for entry in extra:
            if entry is None: continue

            c1.append(entry['item'])
            exact_support = entry['real_support']
            approx_support = entry['approximated_support']
            fill = entry['filled']
            level = entry['level']
            error = -1 * (int(exact_support) - int(approx_support))
            err.append((exact_support, approx_support, error, level, fill))

        if len(err) == 0:
            return False

        df = pd.DataFrame(columns=['items'])
        df['items'] = c1
        df['real_support'], df["approx_support"], df["error"], df["level"], df["fill"] = zip(*err)

        df.sort_values(by=['level', 'error'], inplace=True)

        df.set_index(np.arange(len(df.index)), inplace=True)

        p = 0
        for i in range(len(df["approx_support"].values) - 1):
            item_level = df["level"].values[i]
            item_level_next = df["level"].values[i + 1]
            if item_level != item_level_next:
                if p == 0:
                    plt.axvline(x=i+1, linestyle="--", color="green", label='level divider')
                else:
                    plt.axvline(x=i+1, linestyle="--", color="green")
                p += 1

        plot = list(zip(df["approx_support"].values, df["real_support"].values))

        def connectpoints(p, y):
            x1, x2 = p, p
            y1, y2 = y[p][0], y[p][1]
            plt.plot([x1, x2], [y1, y2], 'k-', color='red')

        for i in range(len(plot)):
            connectpoints(i, plot)

        plt.plot(df["approx_support"].values, 'ro', color="red", label="approx", markersize=4)
        plt.plot(df["real_support"].values, 'ro', color="blue", label="exact", markersize=4)

        plt.ylabel("support")
        plt.xlabel("items")

        plt.axhline(y=self.minsupport, color="green", label="minimum support")

        plt.legend()

        fig = plt.gcf()
        fig.set_size_inches(self.fig_size)
        plt.savefig(self.base_dir + which + "/" + type + "_on_bloom_error_" + self.experiment, dpi=100)
        plt.close()

        self.extra_on_bloom_mean = np.mean(df['error'])
        self.extra_on_bloom_sd = np.std(df['error'])

        df.to_csv(self.base_dir + which + "/" + type + "_on_bloom_" + self.experiment + ".csv")
        return df

    def count_missing(self, one, two):
        diff = []
        for one_item in one:
            is_found = False
            for two_item in two:
                if one_item is not None and two_item is not None and self.lists_match(one_item["items"], two_item["items"]):
                    is_found = True

            if one_item is not None and not is_found:
                # print("not found:", one_item["found"], one_item["items"])
                diff.append(one_item)

        # print("--------------------------------------------")

        return diff

    def lists_match(self, lst1, lst2):
        return set(lst1) == set(lst2)
        # if len(lst1) != len(lst2): return False
        #
        # for i in range(len(lst1)):
        #     if lst1[i] != lst2[i]:
        #         return False
        #
        # return True

    @property
    def data(self):
        return self.df


class ExperimentStatistics:
    def __init__(self, collected=None):

        if collected is None:
            return

        self.base_dir = 'stats/'

        df = pd.DataFrame(collected, columns=[
            'dataset',
            'capacity', 'error_rate', 'm', 'k', 'hash_fn', 'minsup',
            'found_exact', 'found_bloom', 'missing_from_exact',
            'extra_on_bloom', 'extra_on_bloom_mean', 'extra_on_bloom_sd',
            'false_positive %', 'mean_error_support', 'sd_error_support',
            'generated_candidates_exact', 'generated_candidates_bloom',
            'time_bloom', 'initialization_bloom', 'time_exact', 'initialization_exact', 'factor_of_improvement',
            'fit_function_bloom',
            # 'fit_function_hll'
        ])

        df.to_csv(self.base_dir + "all_experiments.csv")

