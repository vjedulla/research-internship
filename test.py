from hyperloglog.hll import HyperLogLog
import numpy as np

import random
import string
import time

from pybloom import BloomFilter

#
# def random_string(size=4, chars=string.ascii_uppercase + string.digits):
#     return ''.join(random.choice(chars) for _ in range(size))
#
# k = 20
#
# kmv = KMinValues(k)
# lst = []
#
# a = 5
# b = 15
#
# for i in range(random.randint(a, b)):
#     val = random_string()
#     lst.append(val)
#     kmv.add(val)
#
# print("Real:", len(lst))
# print(lst)
# print("KMV:", len(kmv))
# print(kmv)



bf = BloomFilter(capacity=3000, error_rate=0.3)

print(bf.bits_per_slice)
print(bf.num_slices)
print(len(bf.bitarray))
print(bf)
exit()


# one = HyperLogLog(0.05)
# two = HyperLogLog(0.05)
# three = HyperLogLog(0.05)
# four = HyperLogLog(0.05)
#
#
# one.add("A")
# one.add("c")
#
# two.add("A")
# two.add("b")
#
# three.add("A")
# four.add("A")
#
# one.intersect(two)
# one.intersect(three)
# one.intersect(four)
#
# print(int(np.round(one.card())))