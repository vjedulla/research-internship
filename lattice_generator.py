import numpy as np
import random
import matplotlib.pyplot as plt

def print_lattice(lattice):
    for t in lattice:
        print(t)


def normalize(lattice):
    min_elem = min(min(i) for i in lattice)
    print("Min element:", min_elem)

    for i in range(0, len(lattice)):
        for j in range(0, len(lattice[i])):
            lattice[i][j] -= min_elem

    return lattice

def vizualize(lattice):
    for i in range(0, len(lattice)):
        plt.plot(lattice[i], [i] * len(lattice[i]))

    plt.show()


def verticalize(lattice):
    tid = 0
    data = {}
    for row in lattice:
        for key in row:
            if key not in data:
                data[key] = [tid]
            else:
                data[key].append(tid)
        tid += 1

def horizontalize(lattice):
    max_elem = max(max(i) for i in lattice)
    print("max:", max_elem)
    lst = [[] for i in range(max_elem+1)]

    print("Length:", len(lst))

    for i in range(0, len(lattice)):
        for j in range(0, len(lattice[i])):
            lst[lattice[i][j]].append(i)

    # print("-"*10)
    # print_lattice(lst)

    return lst

# how many items
desired_elements = 100

# how much of the data will have
# a bigger than minsup
p = 0.6

# desired starting support for p% of the data
# support on each level will fall with the decreasing rate
desired_support = 200
decreasing_rate = 15
span = 200

# how many levels
desired_levels = 3


lattice = []

# starting_random = random.randint(0, 2*desired_support)
#
# for x in range(desired_elements):
#
#     delta = random.randint(0, decreasing_rate)
#
#     if random.uniform(0, 1) < p:
#         # print("here")
#         starting_random = random.randint(0, 100)
#
#     a = random.randint(starting_random + delta, starting_random + decreasing_rate + delta)
#     b = a + desired_support
#     r = list( np.arange(a, b, 1))
#
#     lattice.append(r)


starting_random = random.randint(0, 2*desired_support)
shifting_factor = 0

for x in range(desired_elements):
    delta = random.randint(0, decreasing_rate)

    a = random.randint(starting_random + delta, starting_random + decreasing_rate + delta) + shifting_factor
    b = a + desired_support

    if random.uniform(0,1) < p:
        b += random.randint(0, span)

    r = list(np.arange(a, b, 1))

    lattice.append(r)

    shifting_factor += random.randint(0, int(decreasing_rate/2))


lattice = normalize(lattice)
# print_lattice(lattice)
vizualize(lattice)
horizontal = horizontalize(lattice)

file = open("output.dat", "w")

for row in horizontal:
    for item in row:
        file.write(str(item) + " ")
    file.write("\n")
