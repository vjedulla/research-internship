import numpy as np
# import pybloom
from py_bf import BloomFilter

class ApproxAlgorithmWrapper:

    def __init__(self,module_name, name, properties, intersection=None):
        self.module_name = module_name
        self.algorithm_name = name
        self.items = []

        if module_name == "hyperloglog.hll":
            module = __import__(module_name)
            my_class = getattr(module.hll, name)
        else:
            # module = __import__(module_name)
            # my_class = getattr(module, name)
            my_class = BloomFilter

        if self.algorithm_name == "BloomFilter":
            cap = properties["capacity"]
            err = properties["error_rate"]
            hash_choice = properties["hash_choice"]

            if intersection is not None:
                self.object = intersection
            else:
                self.object = my_class(capacity=cap, error_rate=err, hash_custom=hash_choice)

        elif self.algorithm_name == "HyperLogLog":
            # do nothing if it is not intersection
            # init hyper log log should be called later
            err = properties["error_rate_hll"]
            if intersection is not None:
                self.object = intersection
            else:
                self.object = my_class(error_rate=err)

        else:
            raise Exception("Method not supported yet!")
        self.properties = properties

    def __len__(self):
        if self.algorithm_name == "BloomFilter":
            return self.__aprox_support(self.object)

        if self.algorithm_name == "HyperLogLog":
            return int(np.round(self.object.card()))

        raise Exception("Method not supported yet!")

    def call(self, which_method, options):
        return getattr(self.object, which_method, options)

    def intersection(self, other):
        if self.algorithm_name == "BloomFilter":
            intersection = self.object.intersection(other.object)
            return ApproxAlgorithmWrapper("py_bf", "BloomFilter", self.properties, intersection)
        if self.algorithm_name == "HyperLogLog":
            self.object.intersect(other.object)
            return ApproxAlgorithmWrapper("hyperloglog.hll", "HyperLogLog", self.properties, self.object)

        raise Exception("Method not supported yet!")

    def union(self, other):
        if self.algorithm_name == "BloomFilter":
            intersection = self.object.union(other.object)
            return ApproxAlgorithmWrapper("py_bf", "BloomFilter", self.properties, intersection)

        raise Exception("Method not supported yet!")

    def append(self, value, property=None):
        if self.algorithm_name == "BloomFilter":
            return self.object.add(value)

        if self.algorithm_name == "HyperLogLog":
            return self.object.add(value, property)

        raise Exception("Method not supported yet!")

    def get_object(self, whom):
        return whom.object

    def get_hash_function(self):
        return self.object.hash_choice

    def get_m_k_bloom(self):
        m = self.object.num_bits
        k = self.object.num_slices

        return m, k

    def get_capacity_filled_ratio(self):
        count = self.object.bitarray.count()
        m = self.object.num_bits

        return round((count/m) * 100, 3)

    def __aprox_support(self, obj):
        """
        Approximation of the elements in a BloomFilter

        Reference: https://en.wikipedia.org/wiki/Bloom_filter#Approximating_the_number_of_items_in_a_Bloom_filter
        :param obj:
        :return:
        """
        bitarray = obj.bitarray
        m = obj.num_bits
        k = obj.num_slices

        cnt_ones = bitarray.count()

        approx = -(m / k) * np.log(1 - (cnt_ones / m))

        return int(approx)
