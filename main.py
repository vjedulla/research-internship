import utils
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import time
from wrapper import ApproxAlgorithmWrapper

def equal(itemset_real, itemset_approx):
    return set(itemset_real) == set(itemset_approx)

    # if len(itemset_real) != len(itemset_approx): return False
    #
    # for i in range(len(itemset_real)):
    #     if itemset_real[i] != itemset_approx[i]:
    #         return False
    #
    # return True


def potential_equal(itemset_real, itemset_approx):
    for i in range(len(itemset_real)):
        if len(itemset_real) > len(itemset_approx):
            continue

        if itemset_real[i] != itemset_approx[i]:
            return False

    return True


def potential_candidate(item_support, approximated_list):
    items, real_support = item_support

    for entry in approximated_list:
        if potential_equal(items, entry['items']) and real_support < minsup:
            # utils.found_exact_support_of_approximated_items(items, real_support, entry['support'], entry['level'])
            return True

    return False


def find_filling_of_list_of_items(items_search, approximated_list):
    if len(items_search) == 1:
        return {}

    filled = {}

    for item in items_search:
        for test in approximated_list:
            if test['level'] == 0 and test['items'][0] == item:
                filled[item] = test['filled']

    return filled

def is_on_approximated(item_support, approximated_list, is_approx="exact"):
    items, real_support = item_support

    for entry in approximated_list:
        if equal(items, entry['items']) and real_support < minsup:
            filled = find_filling_of_list_of_items(entry['items'], approximated_list)
            # print('found: ', items)
            if is_approx is "exact":
                utils.found_exact_support_of_approximated_items(items, real_support, entry['support'], entry['level'], filled, is_approx=is_approx)
            elif is_approx is "bloom":
                utils.found_exact_support_of_approximated_items(items, entry['support'], real_support , entry['level'], filled, is_approx=is_approx)
            return True

    return False


def find_m_k(approx_object):
    if not isinstance(approx_object, ApproxAlgorithmWrapper):
        raise ValueError("Error, not a approximate wrapper")

    return approx_object.get_m_k_bloom()


def eclat(prefix, items, is_approx="exact", find_exact_of_list=None, level=0, calc_jaccard=False):
    while items:
        if calc_jaccard:
            i, composite = items.pop()
            itids, jaccard = composite
        else:
            i, itids = items.pop()
            jaccard = -1

        # isupp = len(itids)
        if is_approx is "exact":
            isupp = len(itids)
            filled = 0
        elif is_approx is "bloom":
            filled = itids.get_capacity_filled_ratio()
            s = len(itids)
            isupp = np.round(s)
        elif is_approx is "hll":
            filled = 0
            isupp = np.ceil(len(itids))

        only_discover = False
        if find_exact_of_list is not None:
            only_discover = is_on_approximated((prefix + [i], isupp), find_exact_of_list, is_approx=is_approx)

        if isupp >= minsup or only_discover:

            if is_approx is "exact" and not only_discover:
                utils.add_to_found(prefix + [i], i, isupp, level, jaccard=jaccard, is_approx=is_approx, filled=filled)
            elif is_approx is "bloom" and find_exact_of_list is None:
                utils.add_to_found(prefix + [i], i, isupp, level, jaccard=jaccard, is_approx=is_approx, filled=filled)
            elif is_approx is "hll" and find_exact_of_list is None:
                utils.add_to_found(prefix + [i], i, isupp, level, jaccard=jaccard, is_approx=is_approx, filled=filled)

            suffix = []
            for j, comp in items:
                if calc_jaccard:
                    ojtids, jacc = comp
                    jtids = utils.intersect(itids, ojtids, is_approx)
                    utids = utils.union(itids, ojtids, is_approx)
                    jsupp = len(jtids)
                    calculation_jaccard = round(jsupp / len(utids), 3)
                else:
                    ojtids = comp
                    jtids = utils.intersect(itids, ojtids, is_approx)
                    jsupp = len(jtids)

                if find_exact_of_list is not None:
                    if potential_candidate((prefix + [j], jsupp), find_exact_of_list):
                        # explore this candidate (so is_on_approximate can catch it)
                        if calc_jaccard:
                            suffix.append((j, (jtids, calculation_jaccard)))
                        else:
                            suffix.append((j, jtids))
                        continue

                if len(jtids) >= minsup:
                    if is_approx is "exact":
                        global generated_candidates_exact
                        generated_candidates_exact += 1
                    elif is_approx is "bloom":
                        global generated_candidates_bloom
                        generated_candidates_bloom += 1
                    elif is_approx is "hll":
                        global generated_candidates_hll
                        generated_candidates_hll += 1

                    if calc_jaccard:
                        suffix.append((j, (jtids, calculation_jaccard)))
                    else:
                        suffix.append((j, jtids))

            eclat(prefix + [i], suffix, is_approx, find_exact_of_list, level=(level + 1), calc_jaccard=calc_jaccard)

    if is_approx is "bloom":
        return utils.found_approx_bloom, utils.found_missing_elements_bloom
    elif is_approx is "hll":
        return utils.found_approx_hll, utils.found_missing_elements_hll
    else:
        return utils.found, utils.found_extra_elements_bloom


# all datasets
all_datasets = utils.get_datasets()
print("Will test on " + str(len(all_datasets)) + " datasets")

generated_candidates_bloom = 0
generated_candidates_exact = 0
generated_candidates_hll = 0
collecting = []

for filename, options in all_datasets.items():
    # default_minsup = 290000
    # default_capacity = 400000
    # default_error_rate = 0.3
    default_minsup = options['default_minsup']
    default_capacity = options['default_capacity']
    default_error_rate = options['default_error_rate']

    minsup = default_minsup
    changed_minsup_level = []
    available_hash_functions = ["sha1", "sha256", "mmh3", "mmh3_64"]
    # bloom_hashes = ['default', 'sha3_256']
    bloom_hashes = ['default']

    properties = {
        "capacity": default_capacity,               # for bloom filter
        "error_rate": default_error_rate,           # for bloom filter and hll
        "error_rate_hll": 0.008,           # for bloom filter and hll
        # "hash_choice": 'sha3_256',
        "hash_fn": available_hash_functions[0],
        "real_or_approx": False,                    # true -> real, false -> approx
        "bloom": True,
        "hll": False,
        "calculate_jaccard": False
    }

    # start timer
    ts = []


    experiments = 1
    mean_errors_bloom = []
    variance_error_bloom = []
    std_dev_bloom = []
    mean_errors_hll = []


    # step_capacity = 150000   # 150k
    step_capacity = 2*default_capacity   # 1x, 2x, 3x...nx
    step_error_rate = 0.3
    step_minsup = -100

    print("Testing dataset: " + filename + "...")

    prev = -1

    properties["capacity"] = default_capacity

    for cnt in range(experiments):
        # capacity experiment
        properties['error_rate'] = default_error_rate

        for err in range(1):
            # error_rate experiment
            minsup = default_minsup

            for sup in range(1):
                # minsup experiment
                print("minsup={0}".format(minsup))

                for h in bloom_hashes:
                    properties['hash_choice'] = h
                    x = time.time()
                    items_approx_bloom, items_approx_hll = utils.read_data(filename, properties)
                    xp = time.time() - x

                    if properties['calculate_jaccard']:
                        m, k = find_m_k(items_approx_bloom[0][1][0])
                        hash_choice = items_approx_bloom[0][1][0].get_hash_function()
                    else:
                        m, k = find_m_k(items_approx_bloom[0][1])
                        hash_choice = items_approx_bloom[0][1].get_hash_function()

                    # bloom filter aprox
                    print("Testing bloom filter for capacity = " + str(properties["capacity"]))
                    bloom_time = time.time()
                    found_approx_bloom, _ = eclat([], items_approx_bloom, is_approx="bloom", level=0, calc_jaccard=properties['calculate_jaccard'])
                    utils.modify_found_bloom_filers_filled()
                    print("Generated candidates bloom:", generated_candidates_bloom)
                    a = np.round(time.time() - bloom_time, 3)
                    es_bf = "-Execution time approximation:" + str(a) + "s"
                    print(es_bf)

                    # bloom filter aprox
                    # print("Testing HyperLogLog")
                    # hll_time = time.time()
                    # found_approx_hll, _ = eclat([], items_approx_hll, is_approx="hll", level=0,
                    #                               calc_jaccard=properties['calculate_jaccard'])
                    # print("Generated candidates hll:", generated_candidates_hll)
                    # c = np.round(time.time() - hll_time, 3)
                    # es_hll = "execution time:" + str(c) + "s"
                    # print(es_hll)

                    print("Running exact eclat approach...")
                    properties["real_or_approx"] = True
                    y = time.time()
                    items = utils.read_data(filename, properties)
                    yp = time.time() - y
                    real_time = time.time()
                    found, _ = eclat([], items, find_exact_of_list=None, level=0, calc_jaccard=properties['calculate_jaccard'])
                    print("Generated candidates exact:", generated_candidates_exact)
                    b = np.round(time.time() - real_time, 3)
                    es_rl = "-Execution time exact:" + str(b) + "s"
                    print(es_rl)
                    print("Finished exact")
                    properties["real_or_approx"] = False

                    extra_elements_bloom = utils.get_support(found_approx_bloom, minsup, is_approx="exact", for_approach="bloom")
                    missing_elements_bloom = utils.get_support(found, minsup, is_approx="bloom")

                    # extra_elements_hll = utils.get_support(found_approx_hll, minsup, is_approx="exact", for_approach="hll")
                    # missing_elements_hll = utils.get_support(found, minsup, is_approx="hll")

                    print("Finished, preparing statistics!")
                    experiment = "cap:" + str(default_capacity + (cnt * step_capacity)) + \
                                 "_" + "err:" + str(round(default_error_rate + (err * step_error_rate), 1)).replace('.', '-') + \
                                 "_" + "minsup:" + str(default_minsup + (sup * step_minsup)) + \
                                 "_" + "hash:" + str(h)

                    stats = utils.Statistics(
                        [found, found_approx_bloom, []],
                        [
                            extra_elements_bloom, missing_elements_bloom, [], []
                        ],
                        dataset=filename,
                        experiment=experiment,
                        support=minsup
                    )
                    print("---------------------------------")
                    print("---------------------------------")


                    # get statistic
                    if prev != cnt:
                        prev = cnt
                        if properties["bloom"] is True:
                            mean_errors_bloom.append(stats.mean_error_bloom)
                            std_dev_bloom.append(stats.std_bloom)

                        # ts.append((a, b, c))
                        ts.append((a, b))

                    collect = {
                        'dataset': filename,
                        'capacity': properties['capacity'],
                        'error_rate': properties['error_rate'],
                        'm': m,
                        'k': k,
                        'hash_fn': str(hash_choice),
                        'minsup': minsup,
                        'found_exact': stats.count_exact,
                        'found_bloom': stats.count_bloom_filter,
                        'missing_from_exact': len(stats.missing_exact_bloom),
                        'extra_on_bloom': len(stats.missing_bloom_exact),
                        'extra_on_bloom_mean': stats.extra_on_bloom_mean if hasattr(stats, 'extra_on_bloom_mean') else None,
                        'extra_on_bloom_sd': stats.extra_on_bloom_sd if hasattr(stats, 'extra_on_bloom_sd') else None,
                        'false_positive %': round(float(len(stats.missing_bloom_exact) / stats.count_bloom_filter) * 100.0, 3),
                        'mean_error_support': round(stats.mean_error_bloom, 3),
                        'sd_error_support': round(stats.std_bloom, 3),
                        'generated_candidates_exact': generated_candidates_exact,
                        'generated_candidates_bloom': generated_candidates_bloom,
                        'time_bloom': a,
                        'initialization_bloom': xp,
                        'time_exact': b,
                        'initialization_exact': yp,
                        'factor_of_improvement': round(b/a, 2),
                        'fit_function_bloom': stats.store_fitt_bloom,
                        # 'fit_function_hll': stats.store_fitt_hll if hasattr(stats, 'store_fitt_hll') else None
                    }

                    collecting.append(collect)

                    found_approx_bloom.clear()
                    # found_approx_hll.clear()
                    found.clear()
                    extra_elements_bloom.clear()
                    missing_elements_bloom.clear()
                    stats = None

                    # resets
                    utils.approx_bloom.clear()
                    # utils.approx_hll.clear()
                    utils.data.clear()
                    utils.found_approx_bloom.clear()
                    utils.found.clear()
                    utils.found_extra_elements_bloom.clear()
                    utils.found_missing_elements_bloom.clear()
                    # utils.found_extra_elements_hll.clear()
                    # utils.found_missing_elements_hll.clear()
                    utils.filled_dictionary.clear()
                    generated_candidates_bloom = 0
                    generated_candidates_exact = 0
                    # generated_candidates_hll = 0

                minsup += step_minsup

            properties["error_rate"] = round(properties["error_rate"] + step_error_rate, 1)

        properties["capacity"] += step_capacity

    gs = gridspec.GridSpec(1, 2)

    capacity_range = np.arange(default_capacity, (default_capacity + (experiments * step_capacity)), step_capacity)

    # error curve
    ax = plt.subplot(gs[0, 0])
    plt.plot(capacity_range, mean_errors_bloom, 'bo-', color="red", label="mean error bloom")
    plt.ylabel("error on support")
    plt.xlabel("experiment")

    plt.legend()
    plt.grid()

    ax = plt.subplot(gs[0, 1])
    plt.plot(capacity_range, std_dev_bloom, 'bo-', color="green", label="SD bloom")
    plt.ylabel("error on support")
    plt.xlabel("experiment")

    plt.legend()
    plt.grid()

    fig = plt.gcf()
    fig.set_size_inches(((10, 3)))
    plt.savefig("stats/" + filename.replace('.dat', '') + "/error_curve.png", dpi=100)
    plt.close()

    # timers
    # bloom_ts, real_ts, hll_ts = list(zip(*ts))
    bloom_ts, real_ts = list(zip(*ts))

    plt.plot(capacity_range, bloom_ts, 'bo-', color="green", label="bloom filters timers")
    # plt.plot(capacity_range, hll_ts, 'bo-', color="brown", label="hyperloglog timers")
    plt.plot(capacity_range, real_ts, 'bo-', color="blue", label="real timer")
    plt.ylabel('seconds')
    plt.legend()

    fig = plt.gcf()
    fig.set_size_inches(8, 6)
    plt.savefig("stats/" + filename.replace('.dat', '') + "/timers.png", dpi=100)
    plt.close()

experiment_statistics = utils.ExperimentStatistics(collecting)
