from wrapper import ApproxAlgorithmWrapper
import numpy as np

properties = {
    "capacity": 15000,  # for bloom filter
    "error_rate": 0.008,    # for bloom filter and hll
}

a = ApproxAlgorithmWrapper("pybloom", "BloomFilter", properties)
b = ApproxAlgorithmWrapper("pybloom", "BloomFilter", properties)
c = ApproxAlgorithmWrapper("pybloom", "BloomFilter", properties)


s1 = np.arange(0, 100, 1)
s2 = np.arange(50, 150, 1)
s3 = np.arange(75, 175, 1)

for x in s1:
    a.add(x)
for x in s2:
    b.add(x)
for x in s3:
    c.add(x)


print("size s1:", len(s1))
print("size s2:", len(s2))
print("size s3:", len(s3))


si1 = a.intersection(b)
# si2 = si1.intersection(c)
jaccard_index = len(si1)/len(a.union(b))


print("intersection si1", len(si1))
print("jaccard index", jaccard_index)
# print("intersection si2", len(si2))